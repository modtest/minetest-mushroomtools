-- Tool definitions
minetest.register_tool("mushroomtools:pick_shroom", {
	description = "Mushroom Pickaxe",
	inventory_image = "mushroomtools_pick_shroom.png",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=0,
		groupcaps={
			cracky = {times={[3]=1.60}, uses=13, maxlevel=1},
		},
		damage_groups = {fleshy=2},
	},
	groups = {flammable = 2},
	sound = {breaks = "mushroomtools_tool_breaks"},
})

minetest.register_tool("mushroomtools:shovel_shroom", {
	description = "Mushroom Shovel",
	inventory_image = "mushroomtools_shovel_shroom.png",
	wield_image = "mushroomtools_shovel_shroom.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=0,
		groupcaps={
			crumbly = {times={[1]=3.00, [2]=1.60, [3]=0.60}, uses=13, maxlevel=1},
		},
		damage_groups = {fleshy=2},
	},
	groups = {flammable = 2},
	sound = {breaks = "mushroomtools_tool_breaks"},
})

minetest.register_tool("mushroomtools:axe_shroom", {
	description = "Mushroom Axe",
	inventory_image = "mushroomtools_axe_shroom.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			choppy = {times={[2]=3.00, [3]=1.60}, uses=13, maxlevel=1},
		},
		damage_groups = {fleshy=2},
	},
	groups = {flammable = 2},
	sound = {breaks = "mushroomtools_tool_breaks"},
})

minetest.register_tool("mushroomtools:sword_shroom", {
	description = "Mushroom Sword",
	inventory_image = "mushroomtools_sword_shroom.png",
	tool_capabilities = {
		full_punch_interval = 1,
		max_drop_level=0,
		groupcaps={
			snappy={times={[2]=1.6, [3]=0.40}, uses=13, maxlevel=1},
		},
		damage_groups = {fleshy=2},
	},
	groups = {flammable = 2},
	sound = {breaks = "mushroomtools_tool_breaks"},
})

--Crafting definitions
minetest.register_craft({
	output = "mushroomtools:pick_shroom",
	recipe = {
		{"flowers:mushroom_red", "flowers:mushroom_red", "flowers:mushroom_red"},
		{"", "default:stick", ""},
		{"", "default:stick",  ""}
	}
})

minetest.register_craft({
	output = "mushroomtools:shovel_shroom",
	recipe = {
		{"", "flowers:mushroom_red", ""},
		{"", "default:stick", ""},
		{"", "default:stick",  ""}
	}
})

minetest.register_craft({
	output = "mushroomtools:axe_shroom",
	recipe = {
		{"", "flowers:mushroom_red", "flowers:mushroom_red"},
		{"", "default:stick", "flowers:mushroom_red"},
		{"", "default:stick",  ""}
	}
})

minetest.register_craft({
	output = "mushroomtools:axe_shroom",
	recipe = {
		{"flowers:mushroom_red", "flowers:mushroom_red", ""},
		{"flowers:mushroom_red", "default:stick", ""},
		{"", "default:stick",  ""}
	}
})

minetest.register_craft({
	output = "mushroomtools:sword_shroom",
	recipe = {
		{"", "flowers:mushroom_red", ""},
		{"", "flowers:mushroom_red", ""},
		{"", "default:stick",  ""}
	}
})
